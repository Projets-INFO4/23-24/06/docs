## docs

# Organisation

- [Follow-up sheet](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/06/docs/-/blob/master/06_Follow_Up_Sheet.pdf)
- [Gantt Chart](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/06/docs/-/blob/master/06_Gantt_Chart.pdf)

# Technical documents

- [Report](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/06/docs/-/blob/master/06_Report.pdf)
- [Class diagram (Kenning)](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/06/docs/-/blob/master/06_Class_Diagram_Kenning.png)
- [Class diagram (CWL)](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/06/docs/-/blob/master/06_Class_Diagram_CWL.png)

# Presentation

- [Mid-term presentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/06/docs/-/blob/master/06_Mid-Term_Presentation.pdf)
- [Final presentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/06/docs/-/blob/master/06_Final_Presentation.pdf)
